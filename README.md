![](https://gitlab.com/bstpierre/aklatan/badges/main/pipeline.svg)
![](https://gitlab.com/bstpierre/aklatan/badges/main/coverage.svg)

# aklatan

A personal library & reading list tracker.

*NOTE*: This project is for demonstration purposes, it's not intended to be
used in production as-is. See [Universal
Glue](https://universalglue.dev/) to follow along.

## Description

Aklatan is an app for tracking books that you have read and/or want to
read.

## Badges

COMING SOON

## Installation

COMING SOON

## Contributing

Because this project is a demonstration of app development, contributions
are not accepted.

## License

The code in this project is licensed under the MIT license. See the file
LICENSE in this directory.
