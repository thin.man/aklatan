package main

import (
	"embed"
	"fmt"
	"html/template"
	"io/fs"
	"log"
	"net/http"
	"path/filepath"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
)

//go:embed templates
var tmplEmbed embed.FS

//go:embed static
var staticEmbedFS embed.FS

type staticFS struct {
	fs fs.FS
}

func (sfs *staticFS) Open(name string) (fs.File, error) {
	return sfs.fs.Open(filepath.Join("static", name))
}

var staticEmbed = &staticFS{staticEmbedFS}

func bookIndexGet(c *gin.Context) {
	db := c.Value("database").(*gorm.DB)
	books := []Book{}
	if err := db.Find(&books).Error; err != nil {
		c.AbortWithStatus(http.StatusInternalServerError)
		return
	}
	c.HTML(http.StatusOK, "books/index.html", gin.H{"books": books})
}

func bookNewGet(c *gin.Context) {
	c.HTML(http.StatusOK, "books/new.html", gin.H{})
}

func bookNewPost(c *gin.Context) {
	book := &Book{}
	if err := c.ShouldBind(book); err != nil {
		verrs := err.(validator.ValidationErrors)
		messages := make([]string, len(verrs))
		for i, verr := range verrs {
			messages[i] = fmt.Sprintf(
				"%s is required, but was empty.",
				verr.Field())
		}
		c.HTML(http.StatusBadRequest, "books/new.html",
			gin.H{"errors": messages})
		return
	}
	db := c.Value("database").(*gorm.DB)
	if err := db.Create(&book).Error; err != nil {
		c.AbortWithStatus(http.StatusInternalServerError)
		return
	}
	c.Redirect(http.StatusFound, "/books/")
}

// Middleware to connect the database for each request that uses this
// middleware.
func connectDatabase(db *gorm.DB) gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Set("database", db)
	}
}

func setupRouter(r *gin.Engine, db *gorm.DB) {
	tmpl := template.Must(template.ParseFS(tmplEmbed, "templates/*/*.html"))
	r.SetHTMLTemplate(tmpl)

	r.Use(connectDatabase(db))
	r.StaticFS("/static", http.FS(staticEmbed))
	r.GET("/books/", bookIndexGet)
	r.GET("/books/new", bookNewGet)
	r.POST("/books/new", bookNewPost)
	r.GET("/", func(c *gin.Context) {
		c.Redirect(http.StatusMovedPermanently, "/books/")
	})
}

func setupDatabase(db *gorm.DB) error {
	err := db.AutoMigrate(
		&Book{},
	)
	if err != nil {
		return fmt.Errorf("Error migrating database: %s", err)
	}
	return nil
}

func main() {
	db, err := gorm.Open(sqlite.Open("aklatan.db"), &gorm.Config{})
	if err != nil {
		log.Fatalf("Failed to connect to database: %s", err)
	}
	err = setupDatabase(db)
	if err != nil {
		log.Fatalf("Database setup error: %s", err)
	}
	r := gin.Default()
	setupRouter(r, db)
	err = r.Run(":3000")
	if err != nil {
		log.Fatalf("gin Run error: %s", err)
	}
}
